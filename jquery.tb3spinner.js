/**
 @version: 1.0.8.2
 @author : Beliakov Vladislav [kolerii]
 @site   : http://kolerii.ru/
*/
(function($){

    var tb3Spinner = function(element, options){
      this.$element = $(element);
      this.options  = $.extend({}, $.fn.tb3spinner.defaults, options);

      this.init();

      this.$input   = this.$element.find('.input-spinner');
      this.$buttons = this.$element.find('.btn-spinner');
      this.$element.on('click','.btn-spinner-plus', $.proxy(function(){this.click(true)}, this));
      this.$element.on('click','.btn-spinner-minus', $.proxy(function(){this.click(false)}, this));
      this.$element.on('keyup','.input-spinner', $.proxy(function(){this.isChanged()}, this))

      if(this.options.disabled) this.disable();
    }

    tb3Spinner.prototype = {
      init : function(){
        this.$element.html('<input class="form-control input-sm input-spinner" maxlength="3" type="text"/><span class="btn-group-vertical container-btn-spinner"><button class="btn btn-default btn-spinner btn-spinner-plus" type="button" role="plus"><i class="glyphicon glyphicon-chevron-up chevron-spinner-up"></i></button><button class="btn btn-default btn-spinner  btn-spinner-minus" role="minus" type="button"><i class="glyphicon glyphicon-chevron-down chevron-spinner-down"></i></button></span>');
        this.$element.find('.input-spinner').attr($.extend({}, this.options.attrs, {value:this.options.value}));
      },
      click : function(direction){
          this.$input.val( direction ? parseInt(this.$input.val()) + this.options.step : this.$input.val() - this.options.step);

          this.isChanged();
      },
      value : function( value ){
        if( value == null ) return this.$input.val();

        this.$input.val(parseInt(value));
        this.isChanged();
      },
      isChanged : function(){
          var e = this.$input;
          e.val() > this.options.max ? e.val(this.options.max):0;
          e.val() < this.options.min && this.options.min != null ? e.val(this.options.min):0;
          e.val() == 'NaN' || e.val() == '' ? e.val(0):0;

          if(typeof(this.options.changed) == 'function') this.options.changed(this);
      },
      disable : function(){this.$buttons.attr('disabled','disabled'); this.$input.attr('disabled','disabled')},
      enable  : function(){this.$buttons.removeAttr('disabled'); this.$input.removeAttr('disabled')},
      destroy : function(){this.$element.html('')}
    };

  jQuery.fn.tb3spinner = function(option, value)
  {
    var methodReturn;

    var $set = this.each(function (){
      var $this = $(this);
      var data = $this.data('tb3spinner');
      var options = typeof option === 'object' && option;


      if (!data) $this.data('tb3spinner', (data = new tb3Spinner(this, options)));

      if (typeof option === 'string') methodReturn = data[option](value);
    });

    return methodReturn === undefined ? $set : methodReturn;
  };

  jQuery.fn.tb3spinner.defaults = {
          min   : null,
          max   : 999,
          step  : 1,
          value : 0,
          name  : null,
          changed : null,
          disabled : false,
          attrs : null
        }
})(jQuery);